package main
import (
	"fmt"
)
func main(){
	var numberA int = 4
	var numberB *int = &numberA

	fmt.Println("numnber A (value) : ", numberA)
	fmt.Println("number A (address) : ", &numberA)
	fmt.Println("numberB (value)   :", *numberB)
	fmt.Println("number B (address)", numberB)

	fmt.Println("")

	numberA = 5

	fmt.Println("numberA (value)   :", numberA)
	fmt.Println("numberA (address) :", &numberA)
	fmt.Println("numberB (value)   :", *numberB)
	fmt.Println("numberB (address) :", numberB)
}