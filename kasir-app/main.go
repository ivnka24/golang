package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

var product = map[string]int{
	"ayam crispy":  25000,
	"bebek geprek": 35000,
	"air mineral":  4000,
	"lele geprek":  25000,
	"es teh":       5000,
	"es jeruk":     5000,
}

func main() {
	var totalHarga int
	scanner := bufio.NewScanner(os.Stdin)

	for {
		printMenuPrices(product)

		var choose string
		fmt.Println("Masukkan pilihan menu anda (atau ketik 'selesai' untuk selesai): ")
		scanner.Scan()
		choose = strings.TrimSpace(strings.ToLower(scanner.Text()))

		if choose == "selesai" {
			break
		}

		if price, isExist := product[choose]; isExist {
			totalHarga += price
			fmt.Printf("Menu '%s' ditambahkan. Total harga sekarang: Rp %d\n", choose, totalHarga)
		} else {
			fmt.Println("Menu tidak valid. Silakan pilih menu yang sesuai.")
		}
	}

	fmt.Printf("Total harga yang harus dibayar: Rp %d\n", totalHarga)

	var uangPelanggan int
	fmt.Print("Masukkan jumlah uang pelanggan: Rp ")
	fmt.Scan(&uangPelanggan)

	if uangPelanggan < totalHarga {
		fmt.Println("Maaf, uang pelanggan tidak mencukupi.")
	} else {
		kembalian := uangPelanggan - totalHarga
		if kembalian > 0 {
			fmt.Printf("Terima kasih! Kembalian Anda: Rp %d\n", kembalian)
		} else {
			fmt.Println("Terima kasih! Uang Anda pas, tidak ada kembalian.")
		}
	}
}

func printMenuPrices(m map[string]int) {
	fmt.Println("Menu Prices:")
	for item, price := range m {
		fmt.Printf("%s : Rp %d\n", item, price)
	}
}
