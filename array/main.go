package main

import "fmt"

func main() {
	var names [4]string
	names[0] = "ivanka"
	names[1] = "mauludi"
	names[2] = "juniar"
	names[3] = "oke"
	fmt.Println("=================================")
	fmt.Println(names[0], names[1], names[2], names[3])
	// ---------------------------------------------
	fmt.Println("=================================")
	// var fruits = [4]string{"apple", "grape", "banana", "melon"}
	var fruits = [4]string{
		"apple",
		"grape",
		"banana",
		"melon",
	}

	fmt.Println("Jumlah semua elemen \t\t", len(fruits))
	fmt.Println("Isi semua elemen \t", fruits)
	fmt.Println("=================================")
	var numbers = [...]int{2, 3, 2, 4, 3, 6}
	fmt.Println("data array \t:", numbers)
	fmt.Println("jumlah elemen array \t:", len(numbers))
	fmt.Println("=================================")
	var numbers1 = [2][3]int{[3]int{3, 2, 3}, [3]int{3, 4, 5}}
	var numbers2 = [2][3]int{{3, 2, 3}, {3, 4, 5}}
	fmt.Println("numbers1", numbers1)
	fmt.Println("numbers2", numbers2)
	fmt.Println("=================================")
	for i := 0; i < len(fruits); i++ {
		fmt.Printf("elemen %d : %s\n", i, fruits[i])
	}
	fmt.Println("=================================")
	for i, fruits := range fruits {
		fmt.Printf("elemen %d : %s\n", i, fruits)
	}
	fmt.Println("=================================")
	for _, fruit := range fruits{
		fmt.Printf("nama buah : %s\n", fruit)
	}
	fmt.Println("=================================")
	var fruits1 = make([]string, 2)
	fruits1[0] = "apple"
	fruits1[1] = "mango"
	fmt.Println(fruits)
}
