package main

import "fmt"

func main() {
	var fruits = []string{"apple", "grape", "banana", "melon"}
	fmt.Println(fruits[0])
	// slice dengan array & operasi slice
	fmt.Println("===========================================")
	var newFruits = fruits[1:4]
	fmt.Println(newFruits)
	fmt.Println("===========================================")
	var aFruits = fruits[0:2] //apple,grape,banana
	var bFruits = fruits[0:2:2] //grape,banana,melon

	// var aaFruits = aFruits[1:2]
	// var baFruits = bFruits[0:1]
	fmt.Println(fruits)   // [apple grape banana melon]
	fmt.Println(aFruits)  // [apple grape banana]
	 fmt.Println(bFruits)  // [grape banana melon]
	// fmt.Println(aaFruits) // [grape]
	// fmt.Println(baFruits) // [grape]
	// fmt.Println("=================")
	// baFruits[0] = "pinnaple"
	// fmt.Println(fruits)   // [apple pinnaple banana melon]
	// fmt.Println(aFruits)  // [apple pinnaple banana]
	// fmt.Println(bFruits)  // [pinnaple banana melon]
	// fmt.Println(aaFruits) // [pinnaple]
	// fmt.Println(baFruits) // [pinnaple]
	// fmt.Println("=================")
	// fmt.Println(len(fruits))
	// fmt.Println(cap(fruits))

	// fmt.Println(len(aFruits))
	// fmt.Println(cap(aFruits))

	// fmt.Println(len(bFruits))
	// fmt.Println(cap(bFruits))
	// fmt.Println("=================")
	// var fruitsm = []string{"apple", "grape", "banana"}
	// var cFrtuits = append(fruitsm, "papaya")
	// fmt.Println(fruitsm)
	// fmt.Println(cFrtuits)
	// // copy
	// fmt.Println("=====================")
	// dst := []string{"potato", "potato", "potato"}
	// src := []string{"watermelon"}
	// n:= copy(dst, src)

	// fmt.Println(dst)
	// fmt.Println(src)
	// fmt.Println(n)
}
