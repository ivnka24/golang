package main
import "fmt"
func main(){
	// const firstName string = "jhon"
	// const lastName = "wick"
	// fmt.Print("halo ", firstName, "!\n")
	// fmt.Print("nice to meet you ", lastName, "!\n")
	// -----------------------------------------------
	// fmt.Println("jhon wick")
	// fmt.Println("jhon", "wick")
	// fmt.Print("jhon wick\n")
	// fmt.Print("jhon ", "wick\n")
	// fmt.Print("jhon", " ", "wick\n")
	// -----------------------------------------------
	const(
		square = "kotak"
		isToday bool = true
		numeric uint8 = 1
		floatNum = 2.2
	)
	fmt.Println("===============")
	fmt.Println(square)
	fmt.Println(isToday)
	fmt.Println(numeric)
	fmt.Println(floatNum)
	const(
		a = "konstanta"
		b
	)
	fmt.Println("===============")
	fmt.Println(a)
	fmt.Println(b)
	const(
		today string = "senin"
		sekarang
		isToday2 = true
	)
	fmt.Println("==============")
	fmt.Println(today)
	fmt.Println(sekarang)
	fmt.Println(isToday2)
	const satu, dua = 1, 2
	const three, four string = "tiga", "empat"
	fmt.Println("==============")
	fmt.Println(satu)
	fmt.Println(dua)
	fmt.Println(three)
	fmt.Println(four)
}