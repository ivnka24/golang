package main

import "fmt"

func main() {
	fmt.Println("Pilih Menu Kalkulator Tersebut")
	fmt.Println("Pilih 1 Menu Pertambahan")
	fmt.Println("Pilih 2 Menu Perkalian")
	fmt.Println("Pilih 3 Menu Pembagian")
	fmt.Println("Pilih 4 Menu Pengurangan")
	fmt.Println("Pilih 5 Exit")
	var pilihan int
	fmt.Print("Masukan Angka : ")
	fmt.Scan(&pilihan)
	switch pilihan {
	case 1:
		var a int
		var b int
		fmt.Print("Masukan Angka A :")
		fmt.Scan(&a)
		fmt.Print("Masukan Angka B :")
		fmt.Scan(&b)
		var hasil int
		hasil = a + b
		fmt.Println("Hasil Pertambahan = ", hasil)
	case 2:
		var a int
		var b int
		fmt.Print("Masukan Angka A :")
		fmt.Scan(&a)
		fmt.Print("Masukan Angka B :")
		fmt.Scan(&b)
		var hasil int
		hasil = a * b
		fmt.Println("Hasil Perkalian = ", hasil)
	case 3:
		var a int
		var b int
		fmt.Print("Masukan Angka A :")
		fmt.Scan(&a)
		fmt.Print("Masukan Angka B :")
		fmt.Scan(&b)
		var hasil int
		hasil = a / b
		fmt.Println("Hasil Pembagian = ", hasil)
	case 4:
		var a int
		var b int
		fmt.Print("Masukan Angka A :")
		fmt.Scan(&a)
		fmt.Print("Masukan Angka B :")
		fmt.Scan(&b)
		var hasil int
		hasil = a - b
		fmt.Println("Hasil Pengurangan = ", hasil)
	default:
		fmt.Println("Exit")
	}
}
