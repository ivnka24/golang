package main

import "fmt"

func main() {
	type student struct {
		name  string
		grade int
	}
	var s1 = student{}
	s1.name = "ivanka"
	s1.grade = 10

	var s2 = student{"jojon", 5}

	var s3 = student{name: "jason"}

	fmt.Println("student 1 : ", s1.name)
	fmt.Println("student 2 : ", s2.name)
	fmt.Println("student 3 : ", s3.name)
}