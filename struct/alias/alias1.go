package main

import "fmt"

type person struct {
	name string
	age  int
}
type people = person

func main() {
	var p1 = person{"wick", 21}
	fmt.Println(p1)

	var p2 = people{"vanka", 23}
	fmt.Println(p2)
}
