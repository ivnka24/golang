package main

import "fmt"

func main() {
	type property struct {
		name string `tag1`
		age  int    `tag2`
	}
	var p1 property
	p1.name = "rama"
	p1.age = 22
	fmt.Println(p1)
}
