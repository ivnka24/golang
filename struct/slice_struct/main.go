package main

import "fmt"

type person struct {
	name string
	age  int
}

func main() {
	var allStudents = []person{
		{name: "wick", age: 23},
		{name: "ethan", age: 23},
		{name: "Bourne", age: 22},
	}

	for _, student := range allStudents {
		fmt.Println(student.name, "age is", student.age)
	}
}