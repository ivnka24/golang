package main

import "fmt"

func main() {
	type student struct {
		person struct {
			name string
			age  int
		}
		grade   int
		hobbies []string
	}
	var s student
	s.person.name = "Vanka"
	s.person.age = 23
	s.grade = 10
	s.hobbies = []string{"Mancing", "Game", "Turu Kids!"}
	fmt.Println("Name : ", s.person.name)
	fmt.Println("Age : ", s.person.age)
	fmt.Println("Grade : ", s.grade)
	fmt.Println("Hobbies : ", s.hobbies)

}
