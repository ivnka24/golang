package main

import "fmt"

func main() {
	// var chicken map[string]int
	// chicken = map[string]int{
	// 	"januari": 50,
	// 	"februari": 40,
	// 	"maret": 34,
    // 	"april": 67,
	// }
	// for key, val := range chicken{
	// 	fmt.Println(key, " \t:", val)
	// }
	// var chicken = map[string]int{
	// 	"januari" : 50,
	// 	"februari" : 25,
	// }
	// fmt.Println(len(chicken))
	// fmt.Println(chicken)

	// delete(chicken,"januari")

	// fmt.Println(len(chicken))
	// fmt.Println(chicken)
	// var chicken = map[string]int{"januari" : 50, "februari" : 40}
	// var value, isExist = chicken["januari"]

	// if isExist{
	// 	fmt.Println(value)
	// }else{
	// 	fmt.Println("item is not exist")
	// }
	var chickens = []map[string]string{
		{"name": "chicken blue", "gender": "male"},
		{"name": "chicken red", "gender": "male"},
		{"name": "chicken yellow", "gender": "female"},
	}
	
	for _, chicken := range chickens {
		fmt.Println(chicken["gender"], chicken["name"])
	}
	
	for _, chicken := range chickens {
		fmt.Println(chicken["gender"], chicken["name"])
	}
}