package main
import "fmt"
func main(){
	// var positiveNumber uint8 = 89
	// var negativeNumber = -1243423644
	// fmt.Printf("bilangan positif: %d\n", positiveNumber)
	// fmt.Printf("bilangan negatif: %d\n", negativeNumber)
	//-----------------------------------------------------//
	// var decimalNumber = 2.62
	// fmt.Printf("bilangan desimal: %f\n", decimalNumber)
	// fmt.Printf("bilangan desimal: %.3f\n", decimalNumber)
	//-----------------------------------------------------//
	// var exist bool = false
	// fmt.Printf("exist? %t \n", exist)
	// -----------------------------------------------------
	var message = `Nama saya "Jhon Wick".
	Salam kenal.
	Mari belajar "Golang".`
	fmt.Println(message)
}