package main
import "fmt"
func main(){
	var value = (((2 + 6) % 3) * 4 - 2) /3
	var isEqual = (value == 2)
	fmt.Println("=============")
	fmt.Printf("nilai %d (%t) \n", value, isEqual)
	var left = false
	var right = true
	var leftAndRight = left && right
	var leftOrRight = left || right
	var leftReverse = !left
	fmt.Println("=============")
	fmt.Printf("left && right \t(%t) \n", leftAndRight)
	fmt.Printf("left !! right \t(%t) \n", leftOrRight)
	fmt.Printf("!left \t(%t) \n", leftReverse)
}